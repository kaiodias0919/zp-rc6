***Settings***
Documentation           Cadastro de clientes

Resource                ../resources/base.robot

Suite Setup             Login Session
Suite Teardown          Finish Session

***Test Cases***
Novo cliente
    Dado que acesso o formulário de cadastro de clientes
    E que eu tenho o seguinte cliente:
    ...         Bom Jovi        00000001406         Rua dos Bugs, 1000          11999999999
    Quando faço a inclusão desse cliente
    Então devo ver a notificação:   Cliente cadastrado com sucesso!

Cliente Duplicado
    Dado que acesso o formulário de cadastro de clientes
    E que eu tenho o seguinte cliente:
    ...         Adrian Smith        00000014143         Rua dos Bugs, 2000          11999999991
    Mas este cpf já existe no sistema
    Quando faço a inclusão desse cliente
    Então devo ver a notificação de erro:   Este CPF já existe no sistemas!
   

Campos Obrigatórios
    Dado que acesso o formulário de cadastro de clientes
    E que eu tenho o seguinte cliente:
    ...         ${EMPTY}        ${EMPTY}        ${EMPTY}        ${EMPTY}
    Quando faço a inclusão desse cliente
    Então devo ver mensagens informando que os campos do cadastro de clientes são Obrigatórios

Nome é Obrigatório
    [Tags]          required
    [Template]      Validação de Campos
    ${EMPTY}        48034903094     Rua dos Bugs, 1000      11999999999     Nome é obrigatório

CPF é Obrigatório
    [Tags]          required
    [Template]      Validação de Campos
    Kaio Dias       ${EMPTY}        Rua dos Bugs, 1000      11999999999     CPF é obrigatório

Endereço é Obrigatório
    [Tags]          required
    [Template]      Validação de Campos
    Kaio Dias       48034903094     ${EMPTY}                11999999999     Endereço é obrigatório

Telefone é Obrigatório
    [Tags]          required
    [Template]      Validação de Campos
    Kaio Dias       48034903094     Rua dos Bugs, 1000      ${EMPTY}        Telefone é obrigatório

# foi inserido um novo cenário para criar uma nova Branch no git (telefone_inv)
Telefone incorreto
    [Template]          Validação de Campos
    João da Silva       00000001406      Rua dos Bugs, 1100      1199999999     Telefone inválido


***Keywords***
Validação de Campos 
    [Arguments]     ${nome}     ${cpf}     ${endereco}     ${telefone}     ${saida}

    Dado que acesso o formulário de cadastro de clientes
    E que eu tenho o seguinte cliente:
    ...         ${nome}     ${cpf}     ${endereco}     ${telefone}
    Quando faço a inclusão desse cliente
    Então devo ver o texto:     ${saida}



