***Settings***
Documentation           Login Tentativa

Resource                ../resources/base.robot

# executa uma ou mais Keywords antes da execução de todos os steps de cada caso de testes
Suite Setup              Start Session
# executa uma ou mais Keywords apos a execução de todos os steps de cada caso de testes
Suite Teardown           Finish Session

Test Template           Tentativa de Login

# Comportamento (Dado, Quando, Então)
# BDD só é BDD se o desenvolvedor ler a especificação e desenvolver orientado a ela 
# ATDD (Desenvolvimento guiado por testes de aceitação)

***Keywords***
Tentativa de Login 
    [Arguments]         ${input_email}              ${input_senha}          ${output_mensagem}

    Acesso a página Login
    Submeto minhas credenciais      ${input_email}      ${input_senha}  
    Devo ver um toaster com a mensagem      ${output_mensagem} 

***Test Cases***
Senha Incorreta             admin@zepalheta.com.br      abc123          Ocorreu um erro ao fazer login, cheque as credenciais.
Senha em branco             joao@gmail.com              ${EMPTY}        O campo senha é obrigatório!
Email em branco             ${EMPTY}                    123456          O campo email é obrigatório! 
Email e Senha em branco     ${EMPTY}                    ${EMPTY}        Os campos email e senha não foram preenchidos!  
 



    
